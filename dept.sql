create VIEW dept AS
SELECT Dname,LocationName
from Dept_Locations INNER JOIN Department
 ON (Dept_Locations.Lnumber = Department.Location_id)
 order by ManagerID ASC;

