CREATE TABLE Dept_Locations
(
	Lnumber 	INT 		NOT NULL,
	LocationName	VARCHAR(15)	NOT NULL,
	PRIMARY KEY(Lnumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE Manager
(
	MNumber		INT		NOT Null,
	FName		varchar(30)	not null,
	LName		varchar(30)	not null,
	PersonalID	char(13)	not null,
	Sex		char,
	Bdate		date,
	ComeFrom	varchar(30),
	PRIMARY KEY(MNumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE Department
(
	DNumber		int		not null,
	Dname		varchar(15)	not null,
	ManagerID	int		not null,
	Mgr_start_date	date		not null,
	Location_id	int,
	PRIMARY KEY(Dnumber),
	FOREIGN KEY(Location_id) REFERENCES Dept_Locations(Lnumber),
	FOREIGN KEY(ManagerID) REFERENCES Manager(MNumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE Employee
(
	EmpID		int		not null,
	Fname		varchar(30)	not null,
	Lname		varchar(30)	not null,
	PersonalID	char(13)	not null,
	Bdate		date,
	Address		varchar(30),
	Sex		char,
	Salary		decimal(10,2),
	Dno		int		not null,
	
	PRIMARY KEY(EmpID),
	FOREIGN KEY (Dno) REFERENCES Department(Dnumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


