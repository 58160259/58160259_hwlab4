CREATE VIEW empdept AS
SELECT Fname , Lname , Salary , Dname , LocationName
FROM Employee LEFT JOIN Department on (Employee.EmpID = Department.DNumber)
    LEFT JOIN Dept_Locations on (Department.Location_id = Dept_Locations.Lnumber)

